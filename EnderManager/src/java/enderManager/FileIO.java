package enderManager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.EnumMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FileIO
 */
@WebServlet("/FileIO")
public class FileIO extends HttpServlet {
    private static final long serialVersionUID = 1L;
    enum valueIndex { spawnProtection, maxTickTime,  generatorSettings,
        forceGamemode, allowNether, gamemode, enableQuery, playerIdleTimeout,
        difficulty, spawnMonsters, opPermissionLevel, resourcePackHash,
        announcePlayerAchievements, pvp, snooperEnabled, levelType, hardcore,
        enableCommandBlock, maxPlayers, networkCompressionThreshold, maxWorldSize,
        serverPort, serverIp, spawnNpcs, allowFlight, levelName, viewDistance,
        resourcePack, spawnAnimals, whiteList, generateStructures, onlineMode,
        maxBuildHeight, levelSeed, useNativeTransport, MOTD, enableRcon};
    Map<valueIndex,String> values;

    int valuesSize=37;
    String rootPath;
    String serverName;
    File root;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FileIO() {
        super();
        values = new EnumMap<>(valueIndex.class);
        
        //read in from the .properties file and fill those into values.
        //if there is no file fill in default values
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            // TODO Auto-generated method stub
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        rootPath = getServletContext().getRealPath("/WEB-INF");
        root = new File(rootPath);
        root = root.getParentFile();
        getFormValues(request);
        outputInfo(response);
        serverName = "margowl";
        outputToFile();
        
    }

    /**
     *
     * @param request
     */
    private void getFormValues(HttpServletRequest request){
        serverName=request.getParameter("serverName");
        values.put(valueIndex.spawnProtection, request.getParameter("spawnProtection"));
        values.put(valueIndex.maxTickTime, request.getParameter("maxTickTime"));
        values.put(valueIndex.generatorSettings, request.getParameter("generatorSettings"));
        values.put(valueIndex.forceGamemode, request.getParameter("forceGamemode"));
        values.put(valueIndex.allowNether, request.getParameter("allowNether"));
        values.put(valueIndex.gamemode, request.getParameter("gamemode"));
        values.put(valueIndex.enableQuery, request.getParameter("enableQuery"));
        values.put(valueIndex.playerIdleTimeout, request.getParameter("playerIdleTimeout"));
        values.put(valueIndex.difficulty, request.getParameter("difficulty"));
        values.put(valueIndex.spawnMonsters, request.getParameter("spawnMonsters"));
        values.put(valueIndex.opPermissionLevel, request.getParameter("opPermissionLevel"));
        values.put(valueIndex.resourcePackHash, request.getParameter("resourcePackHash"));
        values.put(valueIndex.announcePlayerAchievements, request.getParameter("announcePlayerAchievements"));
        values.put(valueIndex.pvp, request.getParameter("pvp"));
        values.put(valueIndex.snooperEnabled, request.getParameter("snooperEnabled"));
        values.put(valueIndex.levelType, request.getParameter("levelType"));
        values.put(valueIndex.hardcore, request.getParameter("hardcore"));
        values.put(valueIndex.enableCommandBlock, request.getParameter("enableCommandBlock"));
        values.put(valueIndex.maxPlayers, request.getParameter("maxPlayers"));
        values.put(valueIndex.networkCompressionThreshold, request.getParameter("networkCompressionThreshold"));
        values.put(valueIndex.maxWorldSize, request.getParameter("maxWorldSize"));
        values.put(valueIndex.serverPort, request.getParameter("serverPort"));
        values.put(valueIndex.serverIp, request.getParameter("serverIp"));
        values.put(valueIndex.spawnNpcs, request.getParameter("spawnNpcs"));
        values.put(valueIndex.allowFlight, request.getParameter("allowFlight"));
        values.put(valueIndex.levelName, request.getParameter("levelName"));
        values.put(valueIndex.viewDistance, request.getParameter("viewDistance"));
        if(request.getParameter("resourcePack") != null){
            values.put(valueIndex.resourcePack, request.getParameter("resourcePack"));
        }else{
            values.put(valueIndex.resourcePack, "");
        }
        values.put(valueIndex.spawnAnimals, request.getParameter("spawnAnimals"));
        values.put(valueIndex.whiteList, request.getParameter("whiteList"));
        values.put(valueIndex.generateStructures, request.getParameter("generateStructures"));
        values.put(valueIndex.onlineMode, request.getParameter("onlineMode"));
        values.put(valueIndex.maxBuildHeight, request.getParameter("maxBuildHeight"));
        values.put(valueIndex.levelSeed, request.getParameter("levelSeed"));
        values.put(valueIndex.useNativeTransport, request.getParameter("useNativeTransport"));
        values.put(valueIndex.MOTD, request.getParameter("MOTD"));
        values.put(valueIndex.enableRcon, request.getParameter("enableRcon"));
    }

    /**
     *
     * @param response
     * @throws IOException
     */
    void outputInfo(HttpServletResponse response) throws IOException{
        PrintWriter out = response.getWriter();

        out.println("<html><body>");
        out.println("Server Name : "+ serverName);
        out.write("<br>#Minecraft server properties\n");
        out.write("<br>spawn-protection="+values.get(valueIndex.spawnProtection)+"\n");
        out.write("<br>max-tick-time="+values.get(valueIndex.maxTickTime)+"\n");
        out.write("<br>generator-settings="+values.get(valueIndex.generatorSettings)+"\n");
        out.write("<br>force-gamemode="+values.get(valueIndex.forceGamemode)+"\n");
        out.write("<br>allow-nether="+values.get(valueIndex.allowFlight)+"\n");
        out.write("<br>gamemode="+values.get(valueIndex.gamemode)+"\n");
        out.write("<br>enable-query="+values.get(valueIndex.enableQuery)+"\n");
        out.write("<br>player-idle-timeout="+values.get(valueIndex.playerIdleTimeout)+"\n");
        out.write("<br>difficulty="+values.get(valueIndex.difficulty)+"\n");
        out.write("<br>spawn-monsters="+values.get(valueIndex.spawnMonsters)+"\n");
        out.write("<br>op-permission-level="+values.get(valueIndex.opPermissionLevel)+"\n");
        out.write("<br>resource-pack-hash="+values.get(valueIndex.resourcePackHash)+"\n");
        out.write("<br>announce-player-achievements="+values.get(valueIndex.announcePlayerAchievements)+"\n");
        out.write("<br>pvp="+values.get(valueIndex.pvp)+"\n");
        out.write("<br>snooper-enabled="+values.get(valueIndex.snooperEnabled)+"\n");
        out.write("<br>level-type="+values.get(valueIndex.levelType)+"\n");
        out.write("<br>hardcore="+values.get(valueIndex.hardcore)+"\n");
        out.write("<br>enable-command-block="+values.get(valueIndex.enableCommandBlock)+"\n");
        out.write("<br>max-players="+values.get(valueIndex.maxPlayers)+"\n");
        out.write("<br>network-compression-threshold="+values.get(valueIndex.networkCompressionThreshold)+"\n");
        out.write("<br>max-world-size="+values.get(valueIndex.maxWorldSize)+"\n");
        out.write("<br>server-port="+values.get(valueIndex.serverPort)+"\n");
        out.write("<br>server-ip="+values.get(valueIndex.serverIp)+"\n");
        out.write("<br>spawn-npcs="+values.get(valueIndex.spawnNpcs)+"\n");
        out.write("<br>allow-flight="+values.get(valueIndex.allowFlight)+"\n");
        out.write("<br>level-name="+values.get(valueIndex.levelName)+"\n");
        out.write("<br>view-distance="+values.get(valueIndex.viewDistance)+"\n");
        out.write("<br>resource-pack="+values.get(valueIndex.resourcePack)+"\n");
        out.write("<br>spawn-animals="+values.get(valueIndex.spawnAnimals)+"\n");
        out.write("<br>white-list="+values.get(valueIndex.whiteList)+"\n");
        out.write("<br>generate-structures="+values.get(valueIndex.generateStructures)+"\n");
        out.write("<br>online-mode="+values.get(valueIndex.onlineMode)+"\n");
        out.write("<br>max-build-height="+values.get(valueIndex.maxBuildHeight)+"\n");
        out.write("<br>level-seed="+values.get(valueIndex.levelSeed)+"\n");
        out.write("<br>use-native-transport="+values.get(valueIndex.useNativeTransport)+"\n");
        out.write("<br>motd="+values.get(valueIndex.MOTD)+"\n");
        out.write("<br>enable-rcon="+values.get(valueIndex.enableRcon));
        out.println("</body></html>");
    }

    /**
     * Fills in the value based on the information in the server.properties file
     * @param pathname
     */
    void getInfoFromFile() throws FileNotFoundException, IOException{
        File file = new File(root,"minecraft_servers");
        if(!file.exists()){
           file.mkdir();
           return;
        }
        File serv = new File(file, serverName);
        if(!serv.exists()){
            serv.mkdir();
            return;
        }
        
        File properties = new File(serv, "server.properties");
        if(!properties.exists()){
            return;
        }
        BufferedReader input = new BufferedReader(new FileReader(properties.getAbsolutePath()));
        String tmp = null;
        do{
           try {
               tmp = input.readLine();
           } catch (IOException ex) {
               Logger.getLogger(FileIO.class.getName()).log(Level.SEVERE, null, ex);
               break;
           }
            String[] stuff = tmp.split("=");
            switch(stuff[0]){
            case "spawn-protection":
                values.put(valueIndex.spawnProtection,stuff[1]);
                break;
            case "max-tick-time":
                values.put(valueIndex.maxTickTime,stuff[1]);
                break;
            case "generator-settings":
                values.put(valueIndex.generatorSettings,stuff[1]);
                break;
            case "force-gamemode":
                values.put(valueIndex.forceGamemode,stuff[1]);
                break;
            case "allow-nether":
                values.put(valueIndex.allowFlight,stuff[1]);
                break;
            case "gamemode":
                values.put(valueIndex.gamemode,stuff[1]);
                break;
            case "enable-query":
                values.put(valueIndex.enableQuery,stuff[1]);
                break;
            case "player-idle-timeout":
                values.put(valueIndex.playerIdleTimeout,stuff[1]);
                break;
            case "difficulty":
                values.put(valueIndex.difficulty,stuff[1]);
                break;
            case "spawn-monsters":
                values.put(valueIndex.spawnMonsters,stuff[1]);
                break;
            case "op-permission-level":
                values.put(valueIndex.opPermissionLevel,stuff[1]);
                break;
            case "resource-pack-hash":
                values.put(valueIndex.resourcePackHash,stuff[1]);
                break;
            case "announce-player-achievements":
                values.put(valueIndex.announcePlayerAchievements,stuff[1]);
                break;
            case "pvp":
                values.put(valueIndex.pvp,stuff[1]);
                break;
            case "snooper-enabled":
                values.put(valueIndex.snooperEnabled,stuff[1]);
                break;
            case "level-type":
                values.put(valueIndex.levelType,stuff[1]);
                break;
            case "hardcore":
                values.put(valueIndex.hardcore,stuff[1]);
                break;
            case "enable-command-block":
                values.put(valueIndex.enableCommandBlock,stuff[1]);
                break;
            case "max-players":
                values.put(valueIndex.maxPlayers,stuff[1]);
                break;
            case "network-compression-threshold":
                values.put(valueIndex.networkCompressionThreshold,stuff[1]);
                break;
            case "max-world-size":
                values.put(valueIndex.maxWorldSize,stuff[1]);
                break;
            case "server-port":
                values.put(valueIndex.serverPort,stuff[1]);
                break;
            case "server-ip":
                values.put(valueIndex.serverIp,stuff[1]);
                break;
            case "spawn-npcs":
                values.put(valueIndex.spawnNpcs,stuff[1]);
                break;
            case "allow-flight":
                values.put(valueIndex.allowFlight,stuff[1]);
                break;
            case "level-name":
                values.put(valueIndex.levelName,stuff[1]);
                break;
            case "view-distance":
                values.put(valueIndex.viewDistance,stuff[1]);
                break;
            case "resource-pack":
                values.put(valueIndex.resourcePack,stuff[1]);
                break;
            case "spawn-animals":
                values.put(valueIndex.spawnAnimals,stuff[1]);
                break;
            case "white-list":
                values.put(valueIndex.whiteList,stuff[1]);
                break;
            case "generate-structures":
                values.put(valueIndex.generateStructures,stuff[1]);
                break;
            case "online-mode":
                values.put(valueIndex.onlineMode,stuff[1]);
                break;
            case "max-build-height":
                values.put(valueIndex.maxBuildHeight,stuff[1]);
                break;
            case "level-seed":
                values.put(valueIndex.levelSeed,stuff[1]);
                break;
            case "use-native-transport":
                values.put(valueIndex.useNativeTransport,stuff[1]);
                break;
            case "motd":
                values.put(valueIndex.MOTD,stuff[1]);
                break;
            case "enable-rcon":
                values.put(valueIndex.enableRcon,stuff[1]);
                break;

            }
        }while(true);
        input.close();
    }

    void outputToFile() throws IOException{
        File file = new File(root,"minecraft_servers");
        if(!file.exists()){
           file.mkdir();
        }
        File serv = new File(file, serverName);
        if(!serv.exists()){
            serv.mkdir();
        }
        
        File properties = new File(serv, "server.properties");
        if(!properties.exists()){
            properties.createNewFile();
        }
        BufferedWriter out = new BufferedWriter(new FileWriter(properties.getAbsolutePath()));
        
        out.write("#Minecraft server properties\n");
        out.write("spawn-protection="+values.get(valueIndex.spawnProtection)+"\n");
        out.write("max-tick-time="+values.get(valueIndex.maxTickTime)+"\n");
        out.write("generator-settings="+values.get(valueIndex.generatorSettings)+"\n");
        out.write("force-gamemode="+values.get(valueIndex.forceGamemode)+"\n");
        out.write("allow-nether="+values.get(valueIndex.allowFlight)+"\n");
        out.write("gamemode="+values.get(valueIndex.gamemode)+"\n");
        out.write("enable-query="+values.get(valueIndex.enableQuery)+"\n");
        out.write("player-idle-timeout="+values.get(valueIndex.playerIdleTimeout)+"\n");
        out.write("difficulty="+values.get(valueIndex.difficulty)+"\n");
        out.write("spawn-monsters="+values.get(valueIndex.spawnMonsters)+"\n");
        out.write("op-permission-level="+values.get(valueIndex.opPermissionLevel)+"\n");
        out.write("resource-pack-hash="+values.get(valueIndex.resourcePackHash)+"\n");
        out.write("announce-player-achievements="+values.get(valueIndex.announcePlayerAchievements)+"\n");
        out.write("pvp="+values.get(valueIndex.pvp)+"\n");
        out.write("snooper-enabled="+values.get(valueIndex.snooperEnabled)+"\n");
        out.write("level-type="+values.get(valueIndex.levelType)+"\n");
        out.write("hardcore="+values.get(valueIndex.hardcore)+"\n");
        out.write("enable-command-block="+values.get(valueIndex.enableCommandBlock)+"\n");
        out.write("max-players="+values.get(valueIndex.maxPlayers)+"\n");
        out.write("network-compression-threshold="+values.get(valueIndex.networkCompressionThreshold)+"\n");
        out.write("max-world-size="+values.get(valueIndex.maxWorldSize)+"\n");
        out.write("server-port="+values.get(valueIndex.serverPort)+"\n");
        out.write("server-ip="+values.get(valueIndex.serverIp)+"\n");
        out.write("spawn-npcs="+values.get(valueIndex.spawnNpcs)+"\n");
        out.write("allow-flight="+values.get(valueIndex.allowFlight)+"\n");
        out.write("level-name="+values.get(valueIndex.levelName)+"\n");
        out.write("view-distance="+values.get(valueIndex.viewDistance)+"\n");
        out.write("resource-pack="+values.get(valueIndex.resourcePack)+"\n");
        out.write("spawn-animals="+values.get(valueIndex.spawnAnimals)+"\n");
        out.write("white-list="+values.get(valueIndex.whiteList)+"\n");
        out.write("generate-structures="+values.get(valueIndex.generateStructures)+"\n");
        out.write("online-mode="+values.get(valueIndex.onlineMode)+"\n");
        out.write("max-build-height="+values.get(valueIndex.maxBuildHeight)+"\n");
        out.write("level-seed="+values.get(valueIndex.levelSeed)+"\n");
        out.write("use-native-transport="+values.get(valueIndex.useNativeTransport)+"\n");
        out.write("motd="+values.get(valueIndex.MOTD)+"\n");
        out.write("enable-rcon="+values.get(valueIndex.enableRcon));
        out.close();
    }

    void setDefaultValues(){
        values.put(valueIndex.spawnProtection, "16");
        values.put(valueIndex.maxTickTime, "60000");
        values.put(valueIndex.generatorSettings, "");
        values.put(valueIndex.forceGamemode, "false");
        values.put(valueIndex.allowNether, "true");
        values.put(valueIndex.gamemode, "0");
        values.put(valueIndex.enableQuery, "false");
        values.put(valueIndex.playerIdleTimeout, "0");
        values.put(valueIndex.difficulty, "1");
        values.put(valueIndex.spawnMonsters, "true");
        values.put(valueIndex.opPermissionLevel, "4");
        values.put(valueIndex.resourcePackHash, "");
        values.put(valueIndex.announcePlayerAchievements, "true");
        values.put(valueIndex.pvp, "true");
        values.put(valueIndex.snooperEnabled, "true");
        values.put(valueIndex.levelType, "DEFAULT");
        values.put(valueIndex.hardcore, "false");
        values.put(valueIndex.enableCommandBlock, "false");
        values.put(valueIndex.maxPlayers,"20");
        values.put(valueIndex.networkCompressionThreshold, "256");
        values.put(valueIndex.maxWorldSize, "29999984");
        values.put(valueIndex.serverPort, "25565");
        values.put(valueIndex.serverIp, "");
        values.put(valueIndex.spawnNpcs, "true");
        values.put(valueIndex.allowFlight, "false");
        values.put(valueIndex.levelName, "world");
        values.put(valueIndex.viewDistance, "10");
        values.put(valueIndex.resourcePack, "");
        values.put(valueIndex.spawnAnimals, "true");
        values.put(valueIndex.whiteList, "false");
        values.put(valueIndex.generateStructures, "true");
        values.put(valueIndex.onlineMode, "true");
        values.put(valueIndex.maxBuildHeight, "256");
        values.put(valueIndex.levelSeed, "");
        values.put(valueIndex.useNativeTransport, "true");
        values.put(valueIndex.MOTD, "A Minecraft Server");
        values.put(valueIndex.enableRcon,"false");

    }
}