package enderManager;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author dlewton
 */
public class FileManager extends HttpServlet {

    PrintWriter page = null;
    File webroot = null;
    File curdir = null;
    
    // assumption: curdir is the correct director for table
    // MAKE SURE THIS HOLDS WHENEVER YOU DO SOMETHING HERE
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        // find where the website stuff itself is stored
        File file = new File(getServletContext().getRealPath("/WEB-INF"));
        webroot = file.getParentFile();
        
        // set the current directy to the webroot when we are "not in" a directory
        if(curdir == null) {
            curdir = webroot; // WARNING: THINK POINTERS
        }
        
        // make ready to write HTML to the page for response
        response.setContentType("text/html;charset=UTF-8");
        page = response.getWriter();
        
        processRequest(request, response);
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        // find where the website stuff itself is stored
        File file = new File(getServletContext().getRealPath("/WEB-INF"));
        webroot = file.getParentFile();
        
        // set the current directy to the webroot when we are "not in" a directory
        if(curdir == null) {
            curdir = webroot; // WARNING: THINK POINTERS
        }
        
        // make ready to write HTML to the page for response
        response.setContentType("text/html;charset=UTF-8");
        page = response.getWriter();
        
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /* Stuff below here are utility functions to make the class work */
    
    /* determine action requested, and process accordingly */
    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        String filename = request.getParameter("filename");
        String action = request.getParameter("action");
        
        // form submitted did not have expected input parameters, especially action
        // do nothing and print the page
        if(filename == null || action == null) {
            printPage();
            return;
        }
        
        // respond to the action requested and perform as such
        if(action.equalsIgnoreCase("move")) {
            // Move the File
            
        } else if(action.equalsIgnoreCase("copy")) {
            // Copy the File
        
        } else if(action.equalsIgnoreCase("delete")) {
            // Delete the File
            
        } else if(action.equalsIgnoreCase("navdown")) {
            curdir = new File(curdir, filename);
            
        } else if(action.equalsIgnoreCase("navup")) {
            curdir = curdir.getParentFile();
            
        } else {
            // Not Understood
        }
        
        printPage();
    }
    
    // print the HTML page to the actual HTML page
    private void printPage()
    {
        page.println("<!DOCTYPE html>");
        page.println("<html>");
        
        page.println("<head>");
        printPageHeader();
        printJavascript();
        page.println("</head>");
        
        page.println("<body>");
        printPageBody();
        page.println("</body>");
        page.println("</html>");
    }
    
    // print the HTML page header to the HTML page
    private void printPageHeader() {
        page.println("   <title>Rudimentary Remote File Manager</title>");
        page.println("   <meta charset=\"UTF-8\">                      ");
    }
    
    // print javascript to HTML page. javascript submits hidden form correctly
    private void printJavascript() {
        String script =
        "   <script type=\"text/javascript\">                                    \n" +
        "       function submitform(filename, action)                            \n" +
        "       {                                                                \n" +
        "           document.getElementById(\"input-filename\").value = filename;\n" +
        "           document.getElementById(\"input-action\").value = action;    \n" +
        "           document.forms[\"namelink\"].submit();                       \n" +
        "       }                                                                \n" +
        "   </script>                                                            \n" +
        "";
        
        page.println(script);
    }
    
    // print the form and table to the HTML page
    private void printPageBody() {
        printHiddenForm();
        printFileTable();
    }
    
    // print hidden form to contain "filename" and "action" to perform with it
    // (insecure) way of passing appropriate commands to the website server
    private void printHiddenForm() {
        String form =
        "   <form action=\"FileManager\" method=\"POST\" id=\"namelink\">                         \n" +
        "      <input id=\"input-filename\" type=\"hidden\" name=\"filename\" value=\"nothing\" />\n" +
        "      <input id=\"input-action\" type=\"hidden\" name=\"action\" value=\"nothing\" />    \n" +
        "   </form>                                                                               \n" +
        "";
        
        page.println(form);
    }
    
    // prints the table of files and directories to the HTML page
    private void printFileTable() {
        page.println("<table border=\"1\"");
        
        page.println("<thead>");
        page.println("   <tr>");
        page.println("      <th>Type</th>");
        page.println("      <th>Name</th>");
        page.println("      <th colspan=\"3\">Actions</th>");
        page.println("   </tr>");
        page.println("</thead>");
        
        // Need to do directory stuff
        page.println("<tbody>");
        
        File[] allFiles = curdir.listFiles();
        ArrayList<File> files = new ArrayList<>();
        ArrayList<File> dirs = new ArrayList<>();
        
        /* separate files that are files from files that are directories */
        for(File file : allFiles) {
            if(file.isDirectory()) {
                dirs.add(file);
            }
            else {
                files.add(file);
            }
        }
        
        // print parent directory table row
        printTableRowNavup(curdir.getParent());
        
        // print directory rows in table for each directory in current directory
        for(File file : dirs) {
            printTableRowDirs(file.getName());
        }
        
        // print file rows in table for each file in current directory
        for(File file : files) {
            printTableRowFile(file.getName());
        }
        
        page.println("</tbody>");
        
        page.println("</table>");
    }
    
    /* prints the parent directory table row to the HTML page */
    private void printTableRowNavup(String parname) {
        page.println("<tr>");
        page.println("<td><img alt=\"Dir\" src=\"dir.png\" /></td>");
                
        page.println("<td>");
        page.println("<a href=\"javascript: submitform('" + parname + "', 'navup');\">");
        page.println("../</a>");
        page.println("</td>");
        
        page.println("<td>");
        page.println("<input disabled=\"disabled\" type=\"submit\" value=\"Move\" onclick=\"submitform('" + parname + "', 'move');\" />");
        page.println("</td>");
        
        page.println("<td>");
        page.println("<input disabled=\"disabled\" type=\"submit\" value=\"Copy\" onclick=\"submitform('" + parname + "', 'move');\" />");
        page.println("</td>");
        
        page.println("<td>");
        page.println("<input disabled=\"disabled\" type=\"submit\" value=\"Delete\" onclick=\"submitform('" + parname + "', 'move');\" />");
        page.println("</td>");
        
        page.println("</tr>");
    }
    
    /* prints a table row for a directory to the HTML page */
    private void printTableRowDirs(String dirname) {
        page.println("<tr>");
        page.println("<td><img alt=\"Dir\" src=\"dir.png\" /></td>");
                
        page.println("<td>");
        page.println("<a href=\"javascript: submitform('" + dirname + "', 'navdown');\">");
        page.println(dirname + "</a>");
        page.println("</td>");
        
        page.println("<td>");
        page.println("<input disabled=\"disabled\" type=\"submit\" value=\"Move\" onclick=\"submitform('" + dirname + "', 'move');\" />");
        page.println("</td>");
        
        page.println("<td>");
        page.println("<input disabled=\"disabled\" type=\"submit\" value=\"Copy\" onclick=\"submitform('" + dirname + "', 'move');\" />");
        page.println("</td>");
        
        page.println("<td>");
        page.println("<input type=\"submit\" value=\"Delete\" onclick=\"submitform('" + dirname + "', 'move');\" />");
        page.println("</td>");
        
        page.println("</tr>");
    }
    
    /* prints a table row for a file to the HTML page */
    private void printTableRowFile(String filename) {
        page.println("<tr>");
        page.println("<td><img alt=\"File\" src=\"file.png\" /></td>");
        page.println("<td>" + filename + "</td>");
        
        page.println("<td>");
        page.println("<input type=\"submit\" value=\"Move\" onclick=\"submitform('" + filename + "', 'move');\" />");
        page.println("</td>");
        
        page.println("<td>");
        page.println("<input type=\"submit\" value=\"Copy\" onclick=\"submitform('" + filename + "', 'move');\" />");
        page.println("</td>");
        
        page.println("<td>");
        page.println("<input type=\"submit\" value=\"Delete\" onclick=\"submitform('" + filename + "', 'move');\" />");
        page.println("</td>");
        
        page.println("</tr>");
    }
}
